import Vector2 from "gdxjs/lib/vector2";

export const createPlayer = (world, x, y) => {
  const id = world.createEntity();
  world.addComponent(id, "position", new Vector2(x, y));
  world.addComponent(id, "velocity", new Vector2(0, 0));
  world.addComponent(id, "target", new Vector2(x, y));
  world.addComponent(id, "player", true);
  return id;
};

export const createBullet = (world, x, y, vX, vY) => {
  const id = world.createEntity();
  world.addComponent(id, "position", new Vector2(x, y));
  world.addComponent(id, "velocity", new Vector2(vX, vY));
  world.addComponent(id, "bullet", { expire: 0.5, lifespan: 0 });
  return id;
};
