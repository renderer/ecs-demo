import { createBullet } from "../util/entityFactory";

const createTestInputSystem = world => {
  let firing = true;
  document.addEventListener("keydown", () => (firing = true));
  document.addEventListener("keyup", () => (firing = false));

  world.setSystem(world => {
    if (firing) {
      for (let i = 0; i < 300; i++) {
        createBullet(world, 0, 0, Math.random() * 1000, Math.random() * 1000);
      }
    }
  });
};

export default createTestInputSystem;
