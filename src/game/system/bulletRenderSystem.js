const TYPES = ["bullet", "position"];

const createBulletRenderSystem = world => {
  const context = world.context;

  world.setSystem(world => {
    const ids = world.getEntities(TYPES);
    for (let id of ids) {
      const p = world.getComponent(id, "position");
      context.fillRect(p.x, p.y, 10, 10);
    }
  });
};

export default createBulletRenderSystem;
