const createBulletExpireSystem = world => {
  const BULLET_TYPES = ["bullet"];
  world.setSystem(world => {
    const ids = world.getEntities(BULLET_TYPES);
    const delta = world.getDelta();
    for (let id of ids) {
      const bullet = world.getComponent(id, "bullet");
      bullet.lifespan += delta;
      if (bullet.lifespan >= bullet.expire) {
        world.destroyEntity(id);
      }
    }
  });
};

export default createBulletExpireSystem;
